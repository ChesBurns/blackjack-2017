﻿namespace BlackJack
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.hitButton = new System.Windows.Forms.Button();
            this.standButton = new System.Windows.Forms.Button();
            this.menuPanel = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.playButton = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.topBar = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.rulesPanel = new System.Windows.Forms.Panel();
            this.settingsPanel = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.opponentNum = new System.Windows.Forms.NumericUpDown();
            this.button5 = new System.Windows.Forms.Button();
            this.numPlayersText = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.rulesText = new System.Windows.Forms.Label();
            this.upTen = new System.Windows.Forms.Button();
            this.upFive = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.moneyText = new System.Windows.Forms.Label();
            this.betText = new System.Windows.Forms.Label();
            this.startRound = new System.Windows.Forms.Button();
            this.quit = new System.Windows.Forms.Button();
            this.bettingChange = new System.Windows.Forms.CheckBox();
            this.menuPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.rulesPanel.SuspendLayout();
            this.settingsPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opponentNum)).BeginInit();
            this.SuspendLayout();
            // 
            // hitButton
            // 
            this.hitButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.hitButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.hitButton.FlatAppearance.BorderSize = 0;
            this.hitButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.hitButton.Font = new System.Drawing.Font("InputSans ExLight", 18F);
            this.hitButton.ForeColor = System.Drawing.Color.White;
            this.hitButton.Location = new System.Drawing.Point(12, 442);
            this.hitButton.Name = "hitButton";
            this.hitButton.Size = new System.Drawing.Size(144, 46);
            this.hitButton.TabIndex = 2;
            this.hitButton.Text = "HIT";
            this.hitButton.UseVisualStyleBackColor = false;
            this.hitButton.Click += new System.EventHandler(this.hitButton_Click);
            this.hitButton.MouseEnter += new System.EventHandler(this.hitButton_MouseEnter);
            this.hitButton.MouseLeave += new System.EventHandler(this.hitButton_MouseLeave);
            // 
            // standButton
            // 
            this.standButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.standButton.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.standButton.FlatAppearance.BorderSize = 0;
            this.standButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.standButton.Font = new System.Drawing.Font("InputSans ExLight", 18F);
            this.standButton.ForeColor = System.Drawing.Color.White;
            this.standButton.Location = new System.Drawing.Point(12, 510);
            this.standButton.Name = "standButton";
            this.standButton.Size = new System.Drawing.Size(144, 46);
            this.standButton.TabIndex = 4;
            this.standButton.Text = "STAND";
            this.standButton.UseVisualStyleBackColor = false;
            this.standButton.Click += new System.EventHandler(this.standButton_Click);
            this.standButton.MouseEnter += new System.EventHandler(this.standButton_MouseEnter);
            this.standButton.MouseLeave += new System.EventHandler(this.standButton_MouseLeave);
            // 
            // menuPanel
            // 
            this.menuPanel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.menuPanel.Controls.Add(this.label1);
            this.menuPanel.Controls.Add(this.button3);
            this.menuPanel.Controls.Add(this.button2);
            this.menuPanel.Controls.Add(this.button1);
            this.menuPanel.Controls.Add(this.playButton);
            this.menuPanel.Controls.Add(this.label3);
            this.menuPanel.Controls.Add(this.label2);
            this.menuPanel.Location = new System.Drawing.Point(-1, 132);
            this.menuPanel.Name = "menuPanel";
            this.menuPanel.Size = new System.Drawing.Size(893, 432);
            this.menuPanel.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("InputSans ExLight", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(6, 384);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(872, 27);
            this.label1.TabIndex = 12;
            this.label1.Text = "A Year 12 SDD Game - Developed by Chester Burns";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("InputSans ExLight", 22F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(192, 241);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(500, 55);
            this.button3.TabIndex = 11;
            this.button3.Text = "QUIT";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            this.button3.MouseEnter += new System.EventHandler(this.button3_MouseEnter);
            this.button3.MouseLeave += new System.EventHandler(this.button3_MouseLeave);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("InputSans ExLight", 20F);
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(192, 164);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(245, 61);
            this.button2.TabIndex = 10;
            this.button2.Text = "RULES";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            this.button2.MouseEnter += new System.EventHandler(this.button2_MouseEnter);
            this.button2.MouseLeave += new System.EventHandler(this.button2_MouseLeave);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("InputSans ExLight", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(447, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(245, 61);
            this.button1.TabIndex = 9;
            this.button1.Text = "SETTINGS";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            this.button1.MouseEnter += new System.EventHandler(this.button1_MouseEnter);
            this.button1.MouseLeave += new System.EventHandler(this.button1_MouseLeave);
            // 
            // playButton
            // 
            this.playButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.playButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.playButton.Font = new System.Drawing.Font("InputSans ExLight", 28F);
            this.playButton.ForeColor = System.Drawing.Color.White;
            this.playButton.Location = new System.Drawing.Point(192, 58);
            this.playButton.Name = "playButton";
            this.playButton.Size = new System.Drawing.Size(500, 100);
            this.playButton.TabIndex = 1;
            this.playButton.Text = "P L A Y";
            this.playButton.UseVisualStyleBackColor = false;
            this.playButton.Click += new System.EventHandler(this.button1_Click);
            this.playButton.MouseEnter += new System.EventHandler(this.playButton_MouseEnter);
            this.playButton.MouseLeave += new System.EventHandler(this.playButton_MouseLeave);
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Georgia", 150F);
            this.label3.ForeColor = System.Drawing.Color.Silver;
            this.label3.Location = new System.Drawing.Point(635, 13);
            this.label3.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(326, 229);
            this.label3.TabIndex = 14;
            this.label3.Text = "🂡";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label2.Font = new System.Drawing.Font("Georgia", 150F);
            this.label2.ForeColor = System.Drawing.Color.Silver;
            this.label2.Location = new System.Drawing.Point(-58, 13);
            this.label2.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(326, 229);
            this.label2.TabIndex = 15;
            this.label2.Text = "🂫";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.pictureBox1.Location = new System.Drawing.Point(-4, 433);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(898, 131);
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // topBar
            // 
            this.topBar.BackColor = System.Drawing.Color.Black;
            this.topBar.Font = new System.Drawing.Font("Lulo Clean Outline", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.topBar.ForeColor = System.Drawing.Color.Aqua;
            this.topBar.Location = new System.Drawing.Point(-1, 0);
            this.topBar.Name = "topBar";
            this.topBar.Size = new System.Drawing.Size(893, 131);
            this.topBar.TabIndex = 7;
            this.topBar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Black;
            this.pictureBox2.Location = new System.Drawing.Point(-1, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(893, 131);
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // rulesPanel
            // 
            this.rulesPanel.Controls.Add(this.button4);
            this.rulesPanel.Controls.Add(this.rulesText);
            this.rulesPanel.Location = new System.Drawing.Point(-1, 132);
            this.rulesPanel.Name = "rulesPanel";
            this.rulesPanel.Size = new System.Drawing.Size(893, 432);
            this.rulesPanel.TabIndex = 9;
            this.rulesPanel.Visible = false;
            // 
            // settingsPanel
            // 
            this.settingsPanel.Controls.Add(this.bettingChange);
            this.settingsPanel.Controls.Add(this.label6);
            this.settingsPanel.Controls.Add(this.label5);
            this.settingsPanel.Controls.Add(this.numericUpDown2);
            this.settingsPanel.Controls.Add(this.label4);
            this.settingsPanel.Controls.Add(this.opponentNum);
            this.settingsPanel.Controls.Add(this.button5);
            this.settingsPanel.Controls.Add(this.numPlayersText);
            this.settingsPanel.Location = new System.Drawing.Point(-1, 132);
            this.settingsPanel.Name = "settingsPanel";
            this.settingsPanel.Size = new System.Drawing.Size(893, 432);
            this.settingsPanel.TabIndex = 10;
            this.settingsPanel.Visible = false;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("InputSans ExLight", 20F);
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(478, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(273, 42);
            this.label6.TabIndex = 21;
            this.label6.Text = "OTHER SETTINGS";
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("InputSans ExLight", 20F);
            this.label5.ForeColor = System.Drawing.Color.White;
            this.label5.Location = new System.Drawing.Point(13, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(297, 42);
            this.label5.TabIndex = 16;
            this.label5.Text = "PLAYER SETTINGS";
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.numericUpDown2.Font = new System.Drawing.Font("InputSans ExLight", 12F);
            this.numericUpDown2.ForeColor = System.Drawing.Color.Aqua;
            this.numericUpDown2.Location = new System.Drawing.Point(712, 78);
            this.numericUpDown2.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDown2.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(51, 27);
            this.numericUpDown2.TabIndex = 15;
            this.numericUpDown2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown2.Value = new decimal(new int[] {
            4,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("InputSans ExLight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.White;
            this.label4.Location = new System.Drawing.Point(546, 80);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(164, 26);
            this.label4.TabIndex = 14;
            this.label4.Text = "NO. OF DECKS";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // opponentNum
            // 
            this.opponentNum.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.opponentNum.Font = new System.Drawing.Font("InputSans ExLight", 12F);
            this.opponentNum.ForeColor = System.Drawing.Color.Aqua;
            this.opponentNum.Location = new System.Drawing.Point(298, 78);
            this.opponentNum.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.opponentNum.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.opponentNum.Name = "opponentNum";
            this.opponentNum.Size = new System.Drawing.Size(51, 27);
            this.opponentNum.TabIndex = 13;
            this.opponentNum.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.opponentNum.Value = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.opponentNum.ValueChanged += new System.EventHandler(this.opponentNum_ValueChanged);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.button5.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("InputSans ExLight", 20F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(576, 362);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(302, 61);
            this.button5.TabIndex = 12;
            this.button5.Text = "BACK TO MENU";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            this.button5.MouseEnter += new System.EventHandler(this.button5_MouseEnter);
            this.button5.MouseLeave += new System.EventHandler(this.button5_MouseLeave);
            // 
            // numPlayersText
            // 
            this.numPlayersText.Font = new System.Drawing.Font("InputSans ExLight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.numPlayersText.ForeColor = System.Drawing.Color.White;
            this.numPlayersText.Location = new System.Drawing.Point(77, 80);
            this.numPlayersText.Name = "numPlayersText";
            this.numPlayersText.Size = new System.Drawing.Size(215, 26);
            this.numPlayersText.TabIndex = 1;
            this.numPlayersText.Text = "NO. OF OPPONENTS";
            this.numPlayersText.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("InputSans ExLight", 20F);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(576, 359);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(302, 61);
            this.button4.TabIndex = 11;
            this.button4.Text = "BACK TO MENU";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            this.button4.MouseEnter += new System.EventHandler(this.button4_MouseEnter);
            this.button4.MouseLeave += new System.EventHandler(this.button4_MouseLeave);
            // 
            // rulesText
            // 
            this.rulesText.Font = new System.Drawing.Font("InputSans ExLight", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rulesText.ForeColor = System.Drawing.Color.White;
            this.rulesText.Location = new System.Drawing.Point(9, 6);
            this.rulesText.Name = "rulesText";
            this.rulesText.Size = new System.Drawing.Size(869, 414);
            this.rulesText.TabIndex = 0;
            this.rulesText.Text = "Rules and stuff";
            // 
            // upTen
            // 
            this.upTen.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.upTen.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.upTen.FlatAppearance.BorderSize = 0;
            this.upTen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upTen.Font = new System.Drawing.Font("InputSans ExLight", 12F);
            this.upTen.ForeColor = System.Drawing.Color.White;
            this.upTen.Location = new System.Drawing.Point(253, 531);
            this.upTen.Name = "upTen";
            this.upTen.Size = new System.Drawing.Size(68, 25);
            this.upTen.TabIndex = 12;
            this.upTen.Text = "↑10M";
            this.upTen.UseVisualStyleBackColor = false;
            this.upTen.Click += new System.EventHandler(this.upTen_Click);
            // 
            // upFive
            // 
            this.upFive.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(35)))), ((int)(((byte)(35)))));
            this.upFive.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.upFive.FlatAppearance.BorderSize = 0;
            this.upFive.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.upFive.Font = new System.Drawing.Font("InputSans ExLight", 12F);
            this.upFive.ForeColor = System.Drawing.Color.White;
            this.upFive.Location = new System.Drawing.Point(179, 531);
            this.upFive.Name = "upFive";
            this.upFive.Size = new System.Drawing.Size(68, 25);
            this.upFive.TabIndex = 13;
            this.upFive.Text = "↑5M";
            this.upFive.UseVisualStyleBackColor = false;
            this.upFive.Click += new System.EventHandler(this.upFive_Click);
            // 
            // label7
            // 
            this.label7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.label7.Font = new System.Drawing.Font("InputSans ExLight", 12F);
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.label7.Location = new System.Drawing.Point(204, 435);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(93, 27);
            this.label7.TabIndex = 15;
            this.label7.Text = "MONEY";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // moneyText
            // 
            this.moneyText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.moneyText.Font = new System.Drawing.Font("InputSans ExLight", 20F);
            this.moneyText.ForeColor = System.Drawing.Color.White;
            this.moneyText.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.moneyText.Location = new System.Drawing.Point(162, 458);
            this.moneyText.Name = "moneyText";
            this.moneyText.Size = new System.Drawing.Size(177, 31);
            this.moneyText.TabIndex = 16;
            this.moneyText.Text = "100M";
            this.moneyText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // betText
            // 
            this.betText.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.betText.Font = new System.Drawing.Font("InputSans ExLight", 17F);
            this.betText.ForeColor = System.Drawing.Color.Green;
            this.betText.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.betText.Location = new System.Drawing.Point(162, 500);
            this.betText.Name = "betText";
            this.betText.Size = new System.Drawing.Size(177, 31);
            this.betText.TabIndex = 18;
            this.betText.Text = "BET: 0";
            this.betText.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // startRound
            // 
            this.startRound.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.startRound.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.startRound.Font = new System.Drawing.Font("InputSans ExLight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.startRound.ForeColor = System.Drawing.Color.White;
            this.startRound.Location = new System.Drawing.Point(7, 12);
            this.startRound.Name = "startRound";
            this.startRound.Size = new System.Drawing.Size(300, 50);
            this.startRound.TabIndex = 19;
            this.startRound.Text = "NEW ROUND";
            this.startRound.UseVisualStyleBackColor = false;
            this.startRound.Visible = false;
            this.startRound.Click += new System.EventHandler(this.startRound_Click);
            // 
            // quit
            // 
            this.quit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(5)))), ((int)(((byte)(5)))), ((int)(((byte)(5)))));
            this.quit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.quit.Font = new System.Drawing.Font("InputSans ExLight", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.quit.ForeColor = System.Drawing.Color.White;
            this.quit.Location = new System.Drawing.Point(7, 68);
            this.quit.Name = "quit";
            this.quit.Size = new System.Drawing.Size(300, 50);
            this.quit.TabIndex = 20;
            this.quit.Text = "QUIT";
            this.quit.UseVisualStyleBackColor = false;
            this.quit.Visible = false;
            this.quit.Click += new System.EventHandler(this.quit_Click);
            // 
            // bettingChange
            // 
            this.bettingChange.AutoSize = true;
            this.bettingChange.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.bettingChange.Checked = true;
            this.bettingChange.CheckState = System.Windows.Forms.CheckState.Checked;
            this.bettingChange.Font = new System.Drawing.Font("InputSans ExLight", 14.25F);
            this.bettingChange.ForeColor = System.Drawing.Color.White;
            this.bettingChange.Location = new System.Drawing.Point(607, 109);
            this.bettingChange.Name = "bettingChange";
            this.bettingChange.Size = new System.Drawing.Size(119, 27);
            this.bettingChange.TabIndex = 22;
            this.bettingChange.Text = "BETTING";
            this.bettingChange.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(15)))), ((int)(((byte)(15)))));
            this.ClientSize = new System.Drawing.Size(884, 561);
            this.Controls.Add(this.settingsPanel);
            this.Controls.Add(this.quit);
            this.Controls.Add(this.startRound);
            this.Controls.Add(this.rulesPanel);
            this.Controls.Add(this.menuPanel);
            this.Controls.Add(this.standButton);
            this.Controls.Add(this.hitButton);
            this.Controls.Add(this.topBar);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.upFive);
            this.Controls.Add(this.upTen);
            this.Controls.Add(this.betText);
            this.Controls.Add(this.moneyText);
            this.Controls.Add(this.pictureBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuPanel.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.rulesPanel.ResumeLayout(false);
            this.settingsPanel.ResumeLayout(false);
            this.settingsPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opponentNum)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button hitButton;
        private System.Windows.Forms.Button standButton;
        private System.Windows.Forms.Panel menuPanel;
        private System.Windows.Forms.Button playButton;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.Label topBar;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Panel rulesPanel;
        private System.Windows.Forms.Label rulesText;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel settingsPanel;
        private System.Windows.Forms.Label numPlayersText;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.NumericUpDown opponentNum;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button upTen;
        private System.Windows.Forms.Button upFive;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label moneyText;
        private System.Windows.Forms.Label betText;
        private System.Windows.Forms.Button startRound;
        private System.Windows.Forms.Button quit;
        private System.Windows.Forms.CheckBox bettingChange;
    }
}

