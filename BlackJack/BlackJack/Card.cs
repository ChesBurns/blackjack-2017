﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    class Card
    {
        public int suit { get; set; }
        public int value { get; set; }
        public int number { get; set; }
        public bool isAce { get; set; }
        public bool changeAce { get; set; }
    }

}
