﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack
{
    class Player
    {
        public List<Card> hand = new List<Card>();
        public int total { get; set; }
        public bool bust { get; set; }
        public int hitVal { get; set; }
        public bool play { get; set; }
        public bool twentyOne { get; set; }
        public string name { get; set; }
    }
}
