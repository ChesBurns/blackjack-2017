﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BlackJack
{
    public partial class Form1 : Form
    {
        // Loads the card bitmap
        Bitmap cardimg = 
            new Bitmap("\\Users\\Chester Burns\\Documents\\blackjack-2017\\BlackJack\\BlackJack\\bin\\cards.png");


        // Lists of card characteristics
        Int16[] suits = {0, 1, 2, 3 };
        int[] values = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10 };
        Int16[] numbers = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
        String[] t = { "B", "L", "A", "C", "K", "J", "A", "C", "K" };
        int[] hitVals = {0,0,0,0};

        // Basic integer variables
        int numberDecks = 1;
        int taken;
        int numberPlayers = 5;
        int go = 1;
        int tick;
        int timer2TickNum = 0;
        int screenDivide;
        int round = 1;
        int money = 150;
        int betVal;

        bool game = true;
        bool userWin;
        bool dealerWin;

        // Creating the list of players, cards and totals
        List<Player> players = new List<Player>();
        List<Card> deck = new List<Card>();
        List<Label> totals = new List<Label>();
        List<PictureBox> placeHolders = new List<PictureBox>();
        List<Label> names = new List<Label>();
        List<Player> winners = new List<Player>();
        List<PictureBox> cardPicts = new List<PictureBox>();
        List<Label> bustLabs = new List<Label>();
        List<Label> settingsNameLabels = new List<Label>();
        List<DomainUpDown> settingsCharDomain = new List<DomainUpDown>();

        Random rand = new Random();

        private void newRound()
        {
            startRound.Visible = false;
            quit.Visible = false;

            userWin = false;
            dealerWin = false;
            round = 1;
            go = 0;
            game = true;
            topBar.Location = new Point(-1, 0);
            topBar.Size = new Size(893, 131);
            topBar.Text = "BLACKJACK";
            topBar.Font = new Font("Lulo Clean Outline", 48, FontStyle.Bold);

            foreach (Player p in players)
            {
                p.hand.Clear();
                p.total = 0;
                p.bust = false;
                p.play = false;
                p.twentyOne = false;
            }

            players[0].play = true;

            foreach (PictureBox b in cardPicts)
            {
                Controls.Remove(b);
            }

            foreach (PictureBox c in placeHolders)
            {
                c.Visible = true;
            }

            foreach (Label l in totals)
            {
                l.Text = "0";
                l.ForeColor = Color.White;
            }

            foreach (Label l in bustLabs)
            {
                Controls.Remove(l);
            }

            if (taken >= (numberDecks * 52) - (numberPlayers * 5))
            {
                shuffleDeck();
                taken = 0;
            }

            bustLabs.Clear();
            

            hitButton.Enabled = true;
            standButton.Enabled = true;
            upFive.Enabled = true;
            upTen.Enabled = true;
            deal();
           
            timer1.Enabled = true;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void shuffleDeck()
        {
            for (int i = 0; i <= 100000; i++)
            {
                // Creates a temporary card to store a card while they swap
                Card tempCard = new Card();
                // Selects 2 random cards from the deck
                int card1 = rand.Next(deck.Count());
                int card2 = rand.Next(deck.Count());
            
                // Swaps the card
                tempCard = deck[card1];
                deck[card1] = deck[card2];
                deck[card2] = tempCard;
            }  
        }

        private void createDeck()
        {

            // Repeats for the amount of decks
            for (int i = 0; i < numberDecks; i++)
            {
                // For each suit
                foreach (Int16 suit in suits)
                {
                    // For each value
                    for (int v = 0; v <= 12; v++)
                    {
                        // Creates new card, assigns suit, number and value
                        Card card = new Card();
                        card.suit = suit;
                        card.number = numbers[v];
                        card.value = values[v];

                        if (v == 0)
                        {
                            card.isAce = true;
                        }

                        else
                        {
                            card.isAce = false;
                        }

                        // Adding card to the deck
                        deck.Add(card);
                    }
                }
            }
        }

        private void giveCard(Player p, Label t, int a)
        {
            Card card = deck[taken];

            // Ace value will originally be 1, will change to 11 if total is less than or equal to 10
            if (card.isAce == true && p.total <= 10)
            {
                card.value = 11;
            }
            p.hand.Add(card);
            p.total += card.value;

            // If total is above 21, if there are any aces their value will be taken down to 1 (if it has not been done already)
            if (p.total > 21)
            {
                foreach (Card c in p.hand)
                {
                    if (c.isAce == true && c.changeAce == false && c.value == 11)
                    {
                        c.value = 1;
                        p.total -= 10;
                        c.changeAce = true;
                    }
                }
            }

            t.Text = Convert.ToString(p.total);

            // If total is still 21, the player goes bust
            if (p.total > 21)
            {
                p.play = false;
                p.bust = true;
                t.ForeColor = Color.Maroon;
            }


            // BITMAPPING
                // Creates picturebox, assignes location and size
                // Creates a rectangle
                // Don't know how the rest of it works

            PictureBox pict = new PictureBox();
            pict.BringToFront();
            
            Rectangle cloneRect = new Rectangle(49 + card.number * 260, 49 + card.suit * 366, 220, 319);

            System.Drawing.Imaging.PixelFormat format = cardimg.PixelFormat;
            Bitmap cloneBitmap = cardimg.Clone(cloneRect, format);

            if (p == players[0])
            {
                placeHolders[p.hand.Count - 1].Visible = false;
                pict.Location = new Point(500 + (p.hand.Count - 1) * 75, 447);
                pict.BackColor = Color.FromArgb(5, 5, 5);
                pict.Size = new Size(65, 96);
            }

            else
            {
                pict.Location = new Point((a * screenDivide) - (screenDivide / 2) - 107 + (p.hand.Count - 1) * 43, 200);
                pict.BackColor = Color.FromArgb(15, 15, 15);
                pict.Size = new Size(43, 64);
            }

            pict.Image = cloneBitmap;
            pict.SizeMode = PictureBoxSizeMode.StretchImage;


            Controls.Add(pict);
            cardPicts.Add(pict);

            // Taken shows the amount of cards that have been taken from the deck, this is how the program knows
            // where the next card should be taken from.

            taken++;
            pictureBox1.SendToBack();

            if (p.total == 21)
            {
                t.ForeColor = Color.Aqua;
                p.play = false;
                p.twentyOne = true;

                if (p == players[0])
                {
                    hitButton.Enabled = false;
                    standButton.Enabled = false;
                    upFive.Enabled = false;
                    upTen.Enabled = false;
                }
            }

            else if(p.hand.Count == 5)
            {
                p.play = false;
            }
            if (p.bust == true)
            {
                Label bust = new Label();
                
                bust.Size = new Size(100, 50);
                
                bust.ForeColor = Color.Maroon;
                bust.TextAlign = ContentAlignment.MiddleCenter;
                bust.Text = "B U S T";

                if (p == players[0])
                {
                    bust.BackColor = Color.FromArgb(5, 5, 5);
                    bust.Location = new Point(401, 498);
                    bust.Font = new Font("InputSans ExLight", 14, FontStyle.Regular);
                }
                else
                {
                    bust.BackColor = Color.FromArgb(15, 15, 15);
                    bust.Location = new Point((a * screenDivide) - (screenDivide / 2) - 50, 340);
                    bust.Font = new Font("InputSans ExLight", 12, FontStyle.Regular);
                }
                Controls.Add(bust);
                bustLabs.Add(bust);
                bust.BringToFront();
            }
        }

        private void deal()
        {
            bet(10);
            // Run twice (2 cards each)
            for (int i = 0; i < 2; i++)
            {
                // For each player
                for (int p = 0; p < numberPlayers; p++)
                {
                    giveCard(players[p], totals[p], p);
                }
            }
        }

        private void createPlayers()
        {
            // Creates the amount of players requested
            // Adds them to the players list

            // Creates their total labels
            for(int i = 0; i < numberPlayers; i++)
            {
                if (i == 1)
                {
                    Player dealer = new Player();
                    players.Add(dealer);
                    dealer.hitVal = 17;
                    dealer.name = "Dealer";
                    dealer.hitVal = hitVals[0];
                }
                else
                {
                    Player p = new Player();
                    players.Add(p);
                    if (i == 0)
                    {
                        p.name = "User";
                    }
                    else
                    {
                        p.hitVal = hitVals[i-1];
                        p.name = "Player " + Convert.ToString(i);
                    }
                }

                Label t = new Label();
                t.AutoSize = false;
                if (i == 0)
                {
                    t.Location = new Point(400, 454);
                    t.Size = new Size(100, 47);
                    t.Font = new Font("InputSans ExLight", 36, FontStyle.Bold);
                    t.BackColor = Color.FromArgb(5, 5, 5);


                    for (int x = 0; x < 5; x++)
                    {
                        PictureBox g = new PictureBox();

                        Rectangle clone = new Rectangle(49 + 15 * 260, 45 + 3 * 366, 220, 319);

                        System.Drawing.Imaging.PixelFormat format = cardimg.PixelFormat;
                        Bitmap cbm = cardimg.Clone(clone, format);

                        g.Location = new Point(500 + x * 75, 447);
                        g.BackColor = Color.FromArgb(5, 5, 5);
                        g.Size = new Size(65, 96);
                        g.Image = cbm;
                        g.SizeMode = PictureBoxSizeMode.StretchImage;

                        Controls.Add(g);
                        placeHolders.Add(g);
                    }
                }

                else
                {
                    t.Location = new Point((i*screenDivide)-(screenDivide/2)-25, 300);
                    t.Size = new Size(50, 50);
                    t.Font = new Font("InputSans ExLight", 30, FontStyle.Regular);

                    Label name = new Label();
                    name.AutoSize = false;
                    name.Location = new Point((i * screenDivide) - (screenDivide / 2) - 110, 140);
                    name.Size = new Size(220, 50);
                    name.Font = new Font("InputSans ExLight", 22, FontStyle.Regular);
                    name.Text = "-" + players[i].name + "-";

                    name.FlatStyle = FlatStyle.System;
                    name.TextAlign = ContentAlignment.MiddleCenter;
                    name.ForeColor = Color.White;
                    
                    Controls.Add(name);
                    names.Add(name);
                }
                
                t.FlatStyle = FlatStyle.System;
                t.TextAlign = ContentAlignment.MiddleCenter;
                t.ForeColor = Color.White;
                totals.Add(t);
                Controls.Add(t);

                PictureBox line = new PictureBox();
                line.Size = new Size(2, 433);
                line.BackColor = Color.White;
                if (i > 0 && i < (numberPlayers - 1))
                {
                    line.Location = new Point(screenDivide * i, 0);
                    Controls.Add(line);
                }
                
                
            }
            players[0].play = true;
        }

        private void hitButton_Click(object sender, EventArgs e)
        {
            // If the hit button is clicked, it gives a card to the user
            giveCard(players[0], totals[0], 0);
            if (players[0].play == false)
            {
                hitButton.Enabled = false;
                standButton.Enabled = false;
                upFive.Enabled = false;
                upTen.Enabled = false;
            }


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (settingsCharDomain.Count == 0)
            {
                for (int i = 0; i < numberPlayers - 1; i++)
                {
                    hitVals[i] = rand.Next(12, 21);
                }
            }
            for (int i = 0; i < settingsCharDomain.Count(); i++)
            {
                if (settingsCharDomain[i].Text == "Conservative")
                {
                    hitVals[i] = 12;
                }
                else if (settingsCharDomain[i].Text == "Normal")
                {
                    hitVals[i] = 15;
                }
                else if (settingsCharDomain[i].Text == "Risky")
                {
                    hitVals[i] = 17;
                }
                else if (settingsCharDomain[i].Text == "Stupid")
                {
                    hitVals[i] = 21;
                }
                else if (settingsCharDomain[i].Text == "Random")
                {
                    hitVals[i] = rand.Next(12, 21);
                }
                else
                {
                    hitVals[i] = rand.Next(12, 21);
                }
            }

            menuPanel.Visible = false;
            screenDivide = 884 / (numberPlayers - 1);
            createDeck();
            shuffleDeck();
            createPlayers();
            deal();


            timer1.Enabled = true;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            
            tick++;

            if (round == 2 && players[0].play == false)
            {
                game = false;
                gameFinished();
            }


            if (game == false)
            {
                timer1.Enabled = false;
            }
            // For every 10 ticks
            if (tick % 10 == 0)
            {
                // If the user is not currently playing
                if (players[0].play == false)
                {
                    // If the player is not bust
                    if (players[go].total < players[go].hitVal)
                    {
                        // The player will play
                        players[go].play = true;
                    }
                    computerRound();
                }
            }
           
            //if (players[0].play == false)
            //{
            //    foreach(Label n in names)
            //    {
            //        n.Font = new Font("InputSans ExLight", 28, FontStyle.Regular);
            //    }
            //    names[go-1].Font = new Font("InputSans ExLight", 30, FontStyle.Bold);
            //}
            //else
            //{
            //    foreach (Label n in names)
            //    {
            //        n.Font = new Font("InputSans ExLight", 28, FontStyle.Regular);
            //    }
            //}
        }

        private void computerRound()
        {
            // Creates a temporary player called comp to represent the current player
            Player comp = players[go];

            topBar.Size = new Size(131, 131);
            topBar.Location = new Point(((go - 1) * screenDivide) + (screenDivide/2) - 65, 0);
            topBar.Text = Convert.ToString('\u21e9');

            if (comp.play == true)
            {
                giveCard(comp, totals[go], go);
            }

            if (comp.bust == true || comp.total >= comp.hitVal)
            {
                comp.play = false;
                go++;
            }

            if (go == numberPlayers)
            {
                if (players[0].bust == false && players[0].twentyOne == false && players[0].hand.Count < 5)
                {
                    go = 0;
                    hitButton.Enabled = true;
                    standButton.Enabled = true;
                    upFive.Enabled = true;
                    upTen.Enabled = true;
                    players[0].play = true;
                    round++;
                    topBar.Text = "BlackJack";
                    topBar.Size = new Size(893, 131);
                    topBar.Location = new Point(-1, 0);
                }

                else
                {
                    gameFinished();
                }
            }       
        }

        private void gameFinished()
        {
            topBar.Size = new Size(593, 131);
            topBar.Location = new Point(300, 0);
            topBar.Font = new Font("Lulo Clean Outline", 36, FontStyle.Bold);
            game = false;
            topBar.Text = "Game Over";


            if (players[1].bust == false && players[0].bust == false)
            {
                if (players[0].hand.Count == 5)
                {
                    if (players[1].hand.Count < 5)
                    {
                        userWin = true;
                    }
                }

                else if (players[0].total > players[1].total)
                {
                    userWin = true;
                }
                else if (players[1].total > players[0].total)
                {
                    dealerWin = true;
                }
            }

            else if (players[0].bust == true && players[1].bust == false) { dealerWin = true; }
            else if (players[1].bust == true && players[0].bust == false) { userWin = true; }

            if (userWin == true)
            {
                topBar.Text = "You Win";
                money += betVal * 2;
            }
            else if (dealerWin == true)
            {
                topBar.Text = "Dealer Wins";
            }
            else
            {
                topBar.Text = "Draw";
                money += betVal; 
            }

            moneyText.Text = Convert.ToString(money) + "M";
            betVal = 0;
            betText.Text = "BET: " + Convert.ToString(betVal) + "M";
            startRound.Visible = true;
            quit.Visible = true;
        
        }

        private void standButton_Click(object sender, EventArgs e)
        {
            players[0].play = false;
            standButton.Enabled = false;
            hitButton.Enabled = false;
            upFive.Enabled = false;
            upTen.Enabled = false;
        }


        private void playButton_MouseLeave(object sender, EventArgs e)
        {
            playButton.Font = new Font("InputSans ExLight", 28, FontStyle.Regular);
        }

        private void playButton_MouseEnter(object sender, EventArgs e)
        {
            playButton.Font = new Font("InputSans ExLight", 30, FontStyle.Bold);
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            button2.Font = new Font("InputSans ExLight", 22, FontStyle.Bold);
        }

        private void button2_MouseLeave(object sender, EventArgs e)
        {
            button2.Font = new Font("InputSans ExLight", 20, FontStyle.Regular);
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            button1.Font = new Font("InputSans ExLight", 22, FontStyle.Bold);
        }

        private void button1_MouseLeave(object sender, EventArgs e)
        {
            button1.Font = new Font("InputSans ExLight", 20, FontStyle.Regular);
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            button3.Font = new Font("InputSans ExLight", 24, FontStyle.Bold);
        }

        private void button3_MouseLeave(object sender, EventArgs e)
        {
            button3.Font = new Font("InputSans ExLight", 22, FontStyle.Bold);
        }

        private void hitButton_MouseEnter(object sender, EventArgs e)
        {
            hitButton.Font = new Font("InputSans ExLight", 20, FontStyle.Bold);
        }

        private void hitButton_MouseLeave(object sender, EventArgs e)
        {
            hitButton.Font = new Font("InputSans ExLight", 18, FontStyle.Regular);
        }

        private void standButton_MouseEnter(object sender, EventArgs e)
        {
            standButton.Font = new Font("InputSans ExLight", 20, FontStyle.Bold);
        }

        private void standButton_MouseLeave(object sender, EventArgs e)
        {
            standButton.Font = new Font("InputSans ExLight", 18, FontStyle.Regular);
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            topBar.Text += t[timer2TickNum];
            timer2TickNum++;
            if (timer2TickNum == 9)
            {
                timer2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            rulesPanel.Visible = true;
            menuPanel.Visible = false;
            topBar.Text = "RULES";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            rulesPanel.Visible = false;
            menuPanel.Visible = true;
            topBar.Text = "BLACKJACK";
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            button4.Font = standButton.Font = new Font("InputSans ExLight", 22, FontStyle.Bold);
        }

        private void button4_MouseLeave(object sender, EventArgs e)
        {
            button4.Font = new Font("InputSans ExLight", 20, FontStyle.Regular);
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            settingsPanel.Visible = true;
            menuPanel.Visible = false;
            topBar.Text = "SETTINGS";
            characteristicList();
        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void opponentNum_ValueChanged(object sender, EventArgs e)
        {
            foreach(Label l in settingsNameLabels)
            {
                settingsPanel.Controls.Remove(l);
            }
            foreach(DomainUpDown d in settingsCharDomain)
            {
                settingsPanel.Controls.Remove(d);
            }
            settingsCharDomain.Clear();
            settingsNameLabels.Clear();
            characteristicList();
        }

        private void characteristicList()
        {

            numberPlayers = Convert.ToInt16(opponentNum.Value) + 1;
            for (int i = 0; i < Convert.ToInt16(opponentNum.Value); i++)
            {

                Label l = new Label();
                l.AutoSize = false;
                l.Location = new Point(189, 116 + 30 * i);
                l.Size = new Size(100, 26);
                l.Font = new Font("InputSans ExLight", 11, FontStyle.Regular);
                l.TextAlign = ContentAlignment.MiddleRight;

                if (i == 0)
                {
                    l.Text = "DEALER";
                }

                else
                {
                    l.Text = "PLAYER " + Convert.ToString(i + 1);
                }
                l.ForeColor = Color.White;


                DomainUpDown e = new DomainUpDown();
                e.AutoSize = false;
                e.Location = new Point(298, 118 + 30 * i);
                e.Size = new Size(175, 23);
                e.Font = new Font("InputSans ExLight", 9, FontStyle.Regular);
                e.ForeColor = Color.White;
                e.BackColor = Color.FromArgb(5, 5, 5);
                e.Text = "Change Personality";
                e.Items.Add("Conservative");
                e.Items.Add("Normal");
                e.Items.Add("Risky");
                e.Items.Add("Stupid");
                e.Items.Add("Random");

                settingsCharDomain.Add(e);
                settingsPanel.Controls.Add(e);
                settingsNameLabels.Add(l);
                settingsPanel.Controls.Add(l);
            }
        }

        private void button5_MouseEnter(object sender, EventArgs e)
        {
            button5.Font = standButton.Font = new Font("InputSans ExLight", 22, FontStyle.Bold);
        }

        private void button5_MouseLeave(object sender, EventArgs e)
        {
            button5.Font = standButton.Font = new Font("InputSans ExLight", 20, FontStyle.Regular);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            menuPanel.Visible = true;
            settingsPanel.Visible = false;
            topBar.Text = "BLACKJACK";
            numberDecks = Convert.ToInt16(numericUpDown2.Value);

            if (bettingChange.Checked == false)
            {
                betText.Visible = false;
                moneyText.Visible = false;
                upFive.Visible = false;
                upTen.Visible = false;
                label7.Visible = false;
            }
        }

        private void upFive_Click(object sender, EventArgs e)
        {
            bet(5);
        }

        private void bet(int x)
        {
            if (money >= x)
            {
                money -= x;
                betVal += x;

                betText.Text = "BET: " + Convert.ToString(betVal) + "M";
                moneyText.Text = Convert.ToString(money) + "M";
            }
        }

        private void upTen_Click(object sender, EventArgs e)
        {
            bet(10);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void startRound_Click(object sender, EventArgs e)
        {
            newRound();
        }

        private void quit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
